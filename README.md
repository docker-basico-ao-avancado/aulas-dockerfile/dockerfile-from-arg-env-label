# Dockerfile - FROM, ARG, ENV e LABEL

## Contexto

Este repositório foi criado como conteúdo adicional para a aula [**Dockerfile - FROM, ARG, ENV e LABEL**](https://www.udemy.com/course/docker-do-basico-ao-avancado/learn/lecture/34071410#overview) do curso [**Docker - Básico ao Avançado**](https://www.udemy.com/course/docker-do-basico-ao-avancado/?couponCode=DOCKER_DEZ24), por Daniel Gil.

## Documentação Usada na Aula

- [Dockerfile - FROM](https://docs.docker.com/engine/reference/builder/#from)
- [Dockerfile - ARG](https://docs.docker.com/engine/reference/builder/#arg)
- [Dockerfile - ENV](https://docs.docker.com/engine/reference/builder/#env)
- [Dockerfile - LABEL](https://docs.docker.com/engine/reference/builder/#label)

## Conheça os Cursos

> Quer conhecer um dos cursos?
>
> Aqui você encontra links com cupons de desconto para:

**Docker - Básico ao Avançado**

[![Docker - Básico ao Avançado](https://danielgilcursos.blob.core.windows.net/images/docker-basico-ao-avancado.png)](https://www.udemy.com/course/docker-do-basico-ao-avancado/?couponCode=DOCKER_DEZ24)

**Terraform - Básico ao Avançado**

[![Terraform - Básico ao Avançado](https://danielgilcursos.blob.core.windows.net/images/terraform-basico-ao-avancado.png)](https://www.udemy.com/course/terraform-do-basico-ao-avancado/?couponCode=TERRAFORM_DEZ24)
