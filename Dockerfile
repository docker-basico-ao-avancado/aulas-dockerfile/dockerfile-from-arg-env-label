ARG TAG=latest

FROM alpine:$TAG

ARG CONTAINER_NAME_ARG

ENV CONTAINER_NAME_ENV=${CONTAINER_NAME_ARG:-"Container A"}

LABEL maintainer="Daniel Gil<danielcgil83@gmail.com>"